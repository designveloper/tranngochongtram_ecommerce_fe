import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from './../../services/category.service';
@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  categories$ : Object;
  constructor(private data: CategoryService) { }

  ngOnInit() {
    this.data.getCategories().subscribe(
      data => this.categories$ = data
    )
  }

}
