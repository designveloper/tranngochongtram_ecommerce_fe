import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  categories$: any;
  subCategory$: any;

  constructor(private data: CategoryService) {
    //this.route.params.subscribe(params => this.categoryId$ = params.id);
  }

  ngOnInit() {
    this.data.getCategories().subscribe(
      (data) => {
        this.categories$ = data;

        this.data.getSubCategories().subscribe(
          (data) => {
            this.subCategory$ = data;

            this.categories$.forEach(category => {
              category.subs = this.subCategory$.filter(s => s.category_id === category.id)
            });
          }
        )
        console.log('====================================');
        console.log(this.categories$);
        console.log('====================================');
      }
    );
  }
}
