import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import {ProductServicesService} from '../../../services/product-services.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  products$ : any;

  constructor(private data:ProductServicesService) { }

  ngOnInit() {
    this.data.getProducts().subscribe(
      (data) => this.products$ = data
    )
  }

}
