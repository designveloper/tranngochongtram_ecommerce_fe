import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import {ProductServicesService} from '../../../services/product-services.service';
@Component({
  selector: 'app-filter-section',
  templateUrl: './filter-section.component.html',
  styleUrls: ['./filter-section.component.css']
})
export class FilterSectionComponent implements OnInit {
  productTypes$ : any;
  attributes$: any;

  constructor(private data: ProductServicesService) { }

  ngOnInit() {
    this.data.getProductTypes().subscribe(
      (data) => this.productTypes$ = data
    )

    this.data.getAttributes().subscribe(
      (data) => this.attributes$ = data
    )
  }

}
