import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { HttpClientModule } from'@angular/common/http';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { HeaderComponent } from './components/header/header.component';
import { AppRoutingModule } from './/app-routing.module';
import { NavComponent } from './components/header/nav/nav.component';
import { LogoHeaderComponent } from './components/header/logo-header/logo-header.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginModalComponent } from '../app/components/header/login-modal/login-modal.component';
import { ProductPageComponent } from './components/product-page/product-page.component';
import { FilterSectionComponent } from './components/product-page/filter-section/filter-section.component';
import { ProductsListComponent } from './components/product-page/products-list/products-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavComponent,
    LogoHeaderComponent,
    BodyComponent,
    FooterComponent,
    LoginModalComponent,
    ProductPageComponent,
    FilterSectionComponent,
    ProductsListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
