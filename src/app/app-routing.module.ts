import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductPageComponent } from './components/product-page/product-page.component';
import { BodyComponent } from './components/body/body.component';

const routes: Routes = [
  { path: '', component: BodyComponent },
  { path: 'ladies/dresses', component: ProductPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
