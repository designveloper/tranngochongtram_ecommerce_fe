import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductServicesService {

  constructor(private http: HttpClient) { }

  getProductTypes() {
    return this.http.get('http://localhost:8000/v1/api/product-types')
  }

  getAttributes(){
    return this.http.get('http://localhost:8000/v1/api/attributes')
  }

  getProducts(){
    return this.http.get('http://localhost:8000/v1/api/products?sub-category-id=3')
  }
}
