import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get('http://localhost:8000/v1/api/categories')
  }

  getSubCategories() {
    return this.http.get('http://localhost:8000/v1/api/sub-categories')
  }
}

